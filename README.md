csvconvert
==========

Converts and merges csv files into one readable and standardized csv or an sql-dump.


	Usage: csvconvert.py -v -o output.sql -t sql -d ";" -e latin1 input-file [input-files ...]
		-h | --help
			Prints this help message
		-v | --verbose
			Prints some useless messages in your prompt
		-o filename | --output filename
			Sets where to dump the stuff
		-t format | --type format
			Sets export format. Supported formats are csv and sql
		-d delimiter | --delimiter delimiter
			Delimiter. Frequently , or ;
		-c encoding | --encoding encoding
			Encoding: utf-8, for example, or latin1 maybe...
