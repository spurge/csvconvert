#!/bin/env python3.4
# -*- conding: utf-8 -*-

import csv
import getopt
import sqlite3
import sys


class UsageException(Exception):
    usage = """csvconvert.py Converts csv [files] to something readable format (merged csv or sql)
Usage: csvconvert.py -v -o output.sql -t sql -d ";" -e latin1 input-file [input-files ...]
    -h | --help
        Prints this help message
    -v | --verbose
        Prints some useless messages in your prompt
    -o filename | --output filename
        Sets where to dump the stuff
    -t format | --type format
        Sets export format. Supported formats are csv and sql
    -d delimiter | --delimiter delimiter
        Delimiter. Frequently , or ;
    -c encoding | --encoding encoding
        Encoding: utf-8, for example, or latin1 maybe...
"""

    def __init__(self, val):
        self.value = val

    def __str__(self):
        return "{}\nERROR: {}".format(self.usage, repr(self.value))


def log(verbose, message):
    if verbose:
        print(message, file=sys.stdout)


def import_csv(dbcon, files, delimiter=',', encoding='utf-8', verbose=False):
    readers, cols = [], {}

    for filename in files:
        file = open(filename, 'r', encoding=encoding)
        readers.append({
            'file': file,
            'data': csv.reader(
                file,
                delimiter=delimiter),
            'cols': []
        })

    for read in readers:
        head = next(read['data'])

        for col in head:
            if len(col) > 0:
                read['cols'].append(col)

                if col in cols:
                    cols[col] += 1
                else:
                    cols[col] = 0

        log(verbose, 'Found columns {} in {}'.format(read['cols'],
                                                     read['file']))

    log(verbose, 'Merged columns {}'.format(cols))
    dbcur = dbcon.cursor()
    sql = 'create table "import" ('

    for col in cols:
        sql += '"{}" varchar(128),'.format(col)

    sql = sql[:-1] + ')'

    dbcur.execute('drop table if exists "import"')
    dbcur.execute(sql)

    for read in readers:
        rowcount, mergecount = 0, 0

        for row in read['data']:
            sqlcol, sqldata, sqlupdate = '', '', ''
            colmatch, hasmatched, i = [], False, 0

            for data in row:
                sqlcol += read['cols'][i] + ','
                sqldata += '"{0}",'.format(data)
                sqlupdate += '{0}="{1}",'.format(read['cols'][i], data)

                for col in cols:
                    if col[0] == read['cols'][i] and col[1] > 0:
                        colmatch.append([col[0], data])
                        break
                i += 1

            if len(colmatch):
                sqlmatch = ''

                for colname, colval in colmatch:
                    sqlmatch += '{0}="{1}" AND '.format(colname, colval)

                sqlmatch = sqlmatch[:-5]
                matchresult = dbcur.execute(
                    'select count(*) from "import" where ' + sqlmatch)
                matchrow = matchresult.fetchone()

                if matchrow[0] > 0:
                        hasmatched = True

            if hasmatched:
                dbcur.execute('update "import" set {0} where {1}'.format(
                    sqlupdate[:-1], sqlmatch))
                log(verbose, 'Merging: {}'.format(row))
                mergecount += 1
            else:
                dbcur.execute('insert into "import" ({0}) values ({1})'.format(
                    sqlcol[:-1], sqldata[:-1]))
                log(verbose, 'Adding: {}'.format(row))
                rowcount += 1

        log(verbose,
            '{} new rows found and inserted. {} rows was merged.'.format(
                rowcount, mergecount))
        read['file'].close()

    dbcur.close()
    dbcon.commit()


def export_csv(dbcon, output, verbose=False):
    file = open(output, 'w')
    writer = csv.writer(file)
    resultcount, header = 0, []

    dbcur = dbcon.cursor()
    result = dbcur.execute('select * from "import"')

    for col in result.description:
        header.append(col[0])

    writer.writerow(header)

    for row in result:
        data = []
        for col in row:
            if col is None:
                data.append('')
                log(verbose, 'Exporting {}'.format(row))
            else:
                data.append(col)

        writer.writerow(data)
        resultcount += 1

    log(verbose, '{} rows of data was dumped in {}'.format(
        resultcount, output))
    dbcur.close()
    dbcon.commit()
    file.close()


def export_sql(dbcon, output, verbose=False):
    file = open(output, 'w')
    resultcount = 0

    for line in dbcon.iterdump():
        file.write("{}\n".format(line))
        resultcount += 1
        log(verbose, 'Exporting: {}'.format(line))

    log(verbose, '{} rows of data was dumped in {}'.format(
        resultcount, output))
    file.close()


if __name__ == '__main__':
    verbose, output, filetype = False, None, None
    delimiter, encoding = None, None

    try:
        try:
            opts, args = getopt.getopt(sys.argv[1:], 'hvo:t:d:c:',
                                       ['help', 'verbose', 'output=',
                                        'type=', 'delimiter=', 'encoding='])
            for opt, val in opts:
                if opt in ('-h', '--help'):
                    raise UsageException('HEEEELP!!!!')
                if opt in ('-v', '--verbose'):
                    verbose = True
                if opt in ('-o', '--output'):
                    output = val
                if opt in ('-t', '--type') and (val == 'csv' or val == 'sql'):
                    filetype = val
                if opt in ('-d', '--delimiter'):
                    delimiter = val
                if opt in ('-c', '--encoding'):
                    encoding = val
        except getopt.GetoptError as e:
            raise UsageException('Error while parsing arguments, {}'.format(e))

        if output is None or filetype is None \
           or delimiter is None or encoding is None:
            raise UsageException('Output, type, delimiter and encoding ' +
                                 'arguments are required')

        dbcon = sqlite3.Connection(':memory:')
        import_csv(dbcon, args, delimiter, encoding, verbose)

        if filetype == 'csv':
            export_csv(dbcon, output, verbose)
        elif filetype == 'sql':
            export_sql(dbcon, output, verbose)
        else:
            raise UsageException('Unknown type {}'.format(filetype))

        dbcon.close()
    except Exception as e:
        print(e, file=sys.stderr)
        sys.exit(1)
